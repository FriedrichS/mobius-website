from http.server import HTTPServer, BaseHTTPRequestHandler
from os import curdir, sep

PORT_NUMBER = 8080

class RequestHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		if self.path=="/":
			self.path="/public/index.html"

		try:
			#Check the file extension required and
			#set the right mime type

			sendReply = True
			if self.path.endswith(".html"):
				mimetype='text/html'
			elif self.path.endswith(".jpg"):
				mimetype='image/jpg'
			elif self.path.endswith(".gif"):
				mimetype='image/gif'
			elif self.path.endswith(".js"):
				mimetype='application/javascript'
			elif self.path.endswith(".css"):
				mimetype='text/css'
			elif self.path.endswith("favicon.ico"):
				mimetype='image/x-icon'
				return
			else:
				sendReply=False


			if sendReply == True:
				#Open the static file requested and send it
				self.path = self.path.replace("/page", "/public")
				with open(curdir + sep + self.path) as f:
					self.send_response(200)
					self.send_header('Content-type', mimetype)
					self.end_headers()
					self.wfile.write(f.read().encode())
			return
		except IOError:
			self.send_error(404,'File Not Found: %s' % self.path)

try:
	#Create a web server and define the handler to manage the
	#incoming request
	server = HTTPServer(('localhost', PORT_NUMBER), RequestHandler)
	print('Started httpserver on port ' + str(PORT_NUMBER))
	server.serve_forever()
except KeyboardInterrupt:
	print('^C received, shutting down the web server')
	server.socket.close()