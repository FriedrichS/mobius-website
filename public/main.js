import init, { run_app } from "./pkg/mobius.js";
async function main() {
    await init("./pkg/mobius_bg.wasm");
    run_app();
}
main();