use crate::processor::environment::Environment;
use wasm_bindgen::JsCast;
use web_sys::console;

pub fn draw_graph(env: &Environment, canvas: &web_sys::HtmlCanvasElement) {
    let mut vals = env.history.values();
    let x_axis = vals.next().unwrap();
    let y_axis = vals.next().unwrap();

    let min_x = 0.0;
    let mut max_x = f64::MIN;
    for f in x_axis {
        max_x = max_x.max(*f);
    }

    let min_y = 0.0;
    let mut max_y = f64::MIN;
    for f in y_axis {
        max_y = max_y.max(*f);
    }

    console::log_1(&format!("{} {}", max_x, max_y).into());

    let canvas_width = canvas.width() as f64;
    let canvas_height = canvas.height() as f64;
    let x_step = canvas_width / max_x;
    let y_step = canvas_height / max_y;

    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    context.set_line_width(2.0);
    context.begin_path();

    for (x, y) in x_axis.iter().zip(y_axis.iter()) {
        let x = x * x_step;
        let y = canvas_height - (y * y_step);

        context.line_to(x, y);
    }

    context.stroke();
}
