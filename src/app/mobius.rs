use crate::app::content;
use crate::app::graph_draw;
use crate::app::message::Message;
use crate::processor::environment::Environment;
use wasm_bindgen::JsCast;
use yew::prelude::*;

pub struct Mobius {
    pub link: ComponentLink<Mobius>,
    pub env: Option<Environment>,
    pub code_text: String,
}

impl Component for Mobius {
    type Message = Message;
    type Properties = ();
    fn create(props: Self::Properties, link: ComponentLink<Mobius>) -> Mobius {
        Mobius {
            link,
            env: None,
            code_text: String::from(""),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        let should_render = false;
        match msg {
            Message::Run => {
                self.env = Some(Environment::new(100));
                let env = self.env.as_mut().unwrap();
                Environment::evaluate(env, &self.code_text);

                let document = web_sys::window().unwrap().document().unwrap();
                let canvas = document.get_element_by_id("main-canvas").unwrap();
                let canvas: web_sys::HtmlCanvasElement = canvas
                    .dyn_into::<web_sys::HtmlCanvasElement>()
                    .expect("unable to get cavnas");

                graph_draw::draw_graph(env, &canvas);
            }
            Message::OnCodeChange(val) => {
                self.code_text = val;
            }
        };

        should_render
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        content::get_html(&self)
    }
}
