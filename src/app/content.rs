use crate::app::message::Message;
use crate::app::mobius::Mobius;
use yew::prelude::*;

pub fn get_html(app: &Mobius) -> Html {
    html! {
        <div class="main-div">
            <div class="top-bar">
                <button onclick=app.link.callback(|_| Message::Run)>{"Berechne"}</button>
            </div>
            <div class="left-column">
                <div class="graph-display">
                    <canvas id="main-canvas" width=1920 height=1080 style="width: 100%; height: 100%;"></canvas>
                </div>
                <div class="graph-settings">

                </div>
            </div>
            <div class="text-area">
                <textarea class="code-input" id="code" value={&app.code_text} oninput=app.link.callback(|e: InputData| Message::OnCodeChange(e.value))></textarea>
            </div>
        </div>
    }
}
