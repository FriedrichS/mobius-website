use crate::processor::environment::Environment;
use regex::Regex;
use std::rc::Rc;
use web_sys::console;

#[derive(Debug, Copy, Clone)]
pub enum TokenType {
    Variable,
    Value,
    Equals,
    Add,
    Sub,
    Mul,
    Div,
    Pow,
}

#[derive(Debug)]
pub enum Value {
    String(Rc<String>),
    Number(f64),
    Variable(Rc<String>, f64),
}

#[derive(Debug)]
pub struct Token {
    token_type: TokenType,
    lhs: Option<Box<Token>>,
    rhs: Option<Box<Token>>,
    value: Value,
}

impl Token {
    pub fn new(
        token_type: TokenType,
        lhs: Option<Box<Token>>,
        rhs: Option<Box<Token>>,
        value: Value,
    ) -> Token {
        Token {
            token_type,
            lhs,
            rhs,
            value,
        }
    }

    pub fn evaluate(&self, env: &mut Environment) -> Option<Value> {
        match self.token_type {
            TokenType::Variable => {
                let name = match &self.value {
                    Value::Variable(name, _) => name,
                    _ => panic!("Invalid Operation"),
                };
                let name = (*name).clone();
                let val = env.get_variable(&name);
                Some(Value::Variable(name, val))
            }
            TokenType::Value => Some(self.value.clone()),
            TokenType::Equals => {
                let (var_name, _) = match &self.lhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Variable(name, val) => (Rc::clone(name), *val),
                    _ => panic!("Invalid Operation"),
                };
                let val = match self.rhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    _ => panic!("Invalid Operation"),
                };
                env.set_variable(&(*var_name).clone(), val);
                None
            }
            TokenType::Add => {
                let left = match self.lhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                let right = match self.rhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                Some(Value::Number(left + right))
            }
            TokenType::Sub => {
                let left = match self.lhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                let right = match self.rhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                Some(Value::Number(left - right))
            }
            TokenType::Mul => {
                let left = match self.lhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                let right = match self.rhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                Some(Value::Number(left * right))
            }
            TokenType::Div => {
                let left = match self.lhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                let right = match self.rhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                Some(Value::Number(left / right))
            }
            TokenType::Pow => {
                let left = match self.lhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                let right = match self.rhs.as_ref().unwrap().evaluate(env).unwrap() {
                    Value::Number(val) => val,
                    Value::Variable(_, val) => val,
                    _ => panic!("Invalid Operation"),
                };
                Some(Value::Number(left.powf(right)))
            }
        }
    }
}

impl Clone for Value {
    fn clone(&self) -> Value {
        match &self {
            Value::String(val) => Value::String(Rc::clone(val)),
            Value::Number(val) => Value::Number(*val),
            Value::Variable(name, val) => Value::Variable(Rc::clone(name), *val),
        }
    }
}
