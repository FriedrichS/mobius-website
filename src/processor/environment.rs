use crate::syntax::tokens::Token;
use crate::syntax::tokens::{TokenType, Value};
use regex::Regex;
use std::{collections::HashMap, rc::Rc};
use yew::web_sys::console;

lazy_static! {
    // IMPORTANT: sort by how specific the regex is
    static ref TOKEN_PATTERNS: Vec<(TokenType, Regex)> = vec![
        (TokenType::Equals, Regex::new(r"(.+)=(.+);").unwrap()),
        (TokenType::Add, Regex::new(r"(.+)\+(.+)").unwrap()),
        (TokenType::Sub, Regex::new(r"(.+)\-(.+)").unwrap()),
        (TokenType::Mul, Regex::new(r"(.+)\*(.+)").unwrap()),
        //(TokenType::Div, Regex::new(r"(.+)\/(.+)").unwrap()),
        (TokenType::Pow, Regex::new(r"(.+)\^(.+)").unwrap()),
        (TokenType::Value, Regex::new(r".+").unwrap()),
    ];
}

pub struct Environment {
    variables: HashMap<String, f64>,
    pub history: HashMap<String, Vec<f64>>,
    steps: usize,
}

impl Environment {
    pub fn new(steps: usize) -> Environment {
        Environment {
            variables: HashMap::new(),
            history: HashMap::new(),
            steps,
        }
    }

    pub fn set_variable(&mut self, var: &str, val: f64) {
        *self.variables.entry(var.to_owned()).or_insert(0.0) = val;
    }

    pub fn get_variable(&mut self, var: &str) -> f64 {
        if self.variables.contains_key(var) {
            return *self.variables.get(var).unwrap();
        }
        0.0
    }

    pub fn evaluate(env: &mut Environment, input: &str) {
        let ast = Self::compile_ast(input);
        let mut first_run = true;

        for _ in 0..env.steps {
            for token in &ast {
                token.evaluate(env);
            }

            if first_run {
                first_run = false;

                for (key, value) in &env.variables {
                    env.history.insert(key.to_owned(), Vec::new());
                }
            }

            // copy variables into history
            for (key, value) in &env.variables {
                env.history.get_mut(key).unwrap().push(*value);
            }
        }
    }

    fn compile_ast(input: &str) -> Vec<Token> {
        let mut ast = Vec::new();

        for line in input.lines() {
            let line: String = line.chars().filter(|c| !c.is_whitespace()).collect();
            if line.is_empty() || line.starts_with("//") {
                continue;
            }

            ast.push(Self::tokenize(&line));
        }

        console::log_1(&format!("{:?}", ast).into());

        ast
    }

    fn tokenize(input: &str) -> Token {
        for (token_type, pattern) in &*TOKEN_PATTERNS {
            if let Some(matches) = pattern.captures(input) {
                let mut token_type = *token_type;

                let mut lhs = None;
                let mut rhs = None;
                let num_matches = matches.len();
                if num_matches >= 2 {
                    lhs = Some(Box::new(Self::tokenize(matches.get(1).unwrap().as_str())));
                }
                if num_matches == 3 {
                    rhs = Some(Box::new(Self::tokenize(matches.get(2).unwrap().as_str())));
                }
                let all = matches.get(0).unwrap().as_str();
                let value = if let Ok(num) = all.parse::<f64>() {
                    Value::Number(num)
                } else if lhs.is_none() && rhs.is_none() {
                    token_type = TokenType::Variable;
                    Value::Variable(Rc::new(all.to_owned()), 0.0)
                } else {
                    Value::String(Rc::new(all.to_owned()))
                };

                return Token::new(token_type, lhs, rhs, value);
            }
        }
        panic!("No token matched the input. This should never happen unless the input is empty!")
    }
}
