#![recursion_limit = "1024"]

#[macro_use]
extern crate lazy_static;

mod app;
mod processor;
mod syntax;
mod utils;

use crate::app::mobius::Mobius;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn run_app() -> Result<(), JsValue> {
    yew::start_app::<Mobius>();

    Ok(())
}
