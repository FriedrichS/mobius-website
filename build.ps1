# build the application
wasm-pack build --target web --out-dir .\public\pkg\
Remove-Item .\public\pkg\.gitignore
Remove-Item .\public\pkg\README.md

# bundle the application
rollup --format iife --file ./public/pkg/bundle.js .\public\main.js